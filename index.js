const express = require("express")

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({ extened: true }));

app.get("/home", (req, res) => {

    res.send(`Welcome Home!`);

})

let users = [];

app.post("/create-user", (req, res) => {

    console.log(req.body);

    if (req.body.username !== '' && req.body.password !== '') {

        users.push(req.body)

        res.send(`User has been registered`)
    } else {

        res.send("Please input both username and password")

    }
})

app.get("/users", (req, res) => {

    res.json(users);

})
app.delete("/delete-user", (req, res) => {

    let message;

    for (let i = 0; i < users.length; i++) {

        if (req.body.username == users[i].username) {

            users.splice([i], 1)

            message = `User ${req.body.username} has been deleted.`;

            break;

        } else {

            message = "User does not exist.";
        }

    }

    res.send(message);

});


app.listen(port, () => console.log(`Server running at localhost:${port}`))